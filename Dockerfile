FROM ubuntu:16.04
MAINTAINER Mark McCahill <mccahill@duke.edu>

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root

RUN       apt-get update   &&  apt-get dist-upgrade -y

RUN apt-get install -y --force-yes --no-install-recommends \
        python-numpy \ 
        software-properties-common \
        wget \
        supervisor \
        openssh-server \
        pwgen \
        sudo \
        iputils-ping \
        vim-tiny \
        net-tools \
        lxde \
        lxde-common \
        menu \
        openbox \
        openbox-menu \
        xterm \
        obconf \
        obmenu \
        xfce4-terminal \
        python-xdg \
        scrot \
        x11vnc \
        xvfb \
        gtk2-engines-murrine \
        ttf-ubuntu-font-family \
        firefox \
        firefox-dev \
        libwebkitgtk-3.0-0 \
        xserver-xorg-video-dummy \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /etc/startup.aux/
RUN echo "#Dummy" > /etc/startup.aux/00.sh
RUN chmod +x /etc/startup.aux/00.sh
RUN mkdir -p /etc/supervisor/conf.d
RUN rm /etc/supervisor/supervisord.conf

# create an ubuntu user
#PASS=`pwgen -c -n -1 10`
#PASS=ubuntu
#echo "User: ubuntu Pass: $PASS"
#RUN useradd --create-home --shell /bin/bash --user-group --groups adm,sudo ubuntu

# create an ubuntu user who cannot sudo
RUN useradd --create-home --shell /bin/bash --user-group ubuntu
RUN echo "ubuntu:badpassword" | chpasswd

ADD startup.sh /
ADD cleanup-cruft.sh /
ADD supervisord.conf.xorg /etc/supervisor/supervisord.conf
EXPOSE 6080

ADD openbox-config /openbox-config
RUN cp -r /openbox-config/.config ~ubuntu/
RUN chown -R ubuntu ~ubuntu/.config ; chgrp -R ubuntu ~ubuntu/.config
RUN rm -r /openbox-config

# noVNC
RUN pwd
RUN ls -la
ADD noVNC /noVNC/
RUN ls -la /noVNC/
# make sure the noVNC self.pem cert file is only readable by root
RUN chmod 400 /noVNC/self.pem

# store a password for the VNC service
RUN mkdir /home/root
RUN mkdir /home/root/.vnc
RUN x11vnc -storepasswd foobar /home/root/.vnc/passwd
ADD xorg.conf /etc/X11/xorg.conf


RUN apt-get update && apt-get install -yq \
 build-essential \
 libpng-dev \
 zlib1g-dev \
 libjpeg-dev \
 python-dev \
 imagemagick \
 python-pip \
 python-tk \
 python-numpy \
 python-scipy \
 python-matplotlib \
 git \
 python3-pip \
 python3-tk \
 python3-numpy \
 python3-scipy \
 python3-matplotlib

RUN  pip install pillow
RUN  pip3 install pillow

################ snapgene ###################
RUN apt-get install -y --force-yes libpulse-dev debconf-utils 
RUN apt-get install -y --force-yes gtk2.0
RUN apt-get install -y --force-yes apt-transport-https
RUN echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections
RUN apt-get install -y --force-yes ttf-mscorefonts-installer
RUN wget -O /snapgene_viewer.deb 'https://www.snapgene.com/local/targets/viewer_download.php?os=linux_deb&majorRelease=latest&minorRelease=latest'
RUN dpkg -i /snapgene_viewer.deb
RUN rm  /snapgene_viewer.deb


# get rid of some LXDE & OpenBox cruft that doesn't work and clutters menus
RUN rm /usr/share/applications/display-im6.q16.desktop & \
    rm /usr/share/applications/display-im6.desktop & \
    rm /usr/share/applications/lxterminal.desktop & \
    rm /usr/share/applications/debian-uxterm.desktop & \
    rm /usr/share/applications/x11vnc.desktop & \
    rm /usr/share/applications/lxde-x-www-browser.desktop & \
    ln -s /usr/share/applications/firefox.desktop /usr/share/applications/lxde-x-www-browser.desktop & \
    rm /usr/share/applications/lxde-x-terminal-emulator.desktop  & \
    rm -rf /usr/share/ImageMagick-6



ENTRYPOINT ["/startup.sh"]
